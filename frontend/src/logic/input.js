import Vector from "../../../shared/physics/vector.mjs"
import app from "./game.js"
import System from "../../../shared/systems/system.mjs";

export default new class Input extends System {
    static ScreenToWorldCoordinates(p) {
        return new Vector(
            p.x / app.canvas.scale.x + app.canvas.pivot.x,
            p.y / app.canvas.scale.y + app.canvas.pivot.y
        );
    }

    get mouse() {
        return Input.ScreenToWorldCoordinates(
            new Vector(
                app.renderer.plugins.interaction.mouse.global.x,
                app.renderer.plugins.interaction.mouse.global.y
            )
        );
    }

    get leftMouseDown() {
        return this._mouseState.leftDown
    }

    get rightMouseDown() {
        return this._mouseState.rightDown;
    }

    get leftMousePressedThisFrame() {
        return this._mouseState.leftPressedThisFrame;
    }

    get rightMousePressedThisFrame() {
        return this._mouseState.rightPressedThisFrame;
    }

    get leftMouseReleasedThisFrame() {
        return this._mouseState.leftReleasedThisFrame;
    }

    get rightMouseReleasedThisFrame() {
        return this._mouseState.rightReleasedThisFrame;
    }

    get dragging() {
        return this._mouseState.dragging;
    }

    get dragStartPoint() {
        return this._mouseState.dragStartPoint;
    }

    get startedDraggingThisFrame() {
        return this._mouseState.startedDraggingThisFrame;
    }

    get finishedDraggingThisFrame() {
        return this._mouseState.finishedDraggingThisFrame;
    }

    get touching() {
        return this._touchState.touching;
    }

    get startedTouchingThisFrame() {
        return this._touchState.startedTouchingThisFrame;
    }

    get finishedTouchingThisFrame() {
        return this._touchState.finishedTouchingThisFrame;
    }

    get touchPosition() {
        return this._touchState.touchPosition;
    }

    get touchStartPoint() {
        return this._touchState.touchStartPoint;
    }

    get touchMovedThisFrame() {
        return this._touchState.touchMovedThisFrame;
    }

    get multiTouching() {
        return this._touchState.multiTouching;
    }

    get multiTouches() {
        return this._touchState.multiTouches;
    }

    initialize() {
        this._mouseState = {
            leftDown: false,
            rightDown: false,
            leftPressedThisFrame: false,
            rightPressedThisFrame: false,
            leftReleasedThisFrame: false,
            rightReleasedThisFrame: false,
            dragging: false,
            dragStartPoint: null,
            startedDraggingThisFrame: false,
            finishedDraggingThisFrame: false
        };
        this._touchState = {
            touching: false,
            startedTouchingThisFrame: false,
            finishedTouchingThisFrame: false,
            touchPosition: null,
            touchStartPoint: null,
            scaleGesture: 1,
            touchMovedThisFrame: false,
            multiTouching: false,
            multiTouches: []
        };
        this._pressedKeys = new Set();

        window.addEventListener("keydown", this._keyDown.bind(this), false);
        window.addEventListener("keyup", this._keyUp.bind(this), false);

        app.renderer.plugins.interaction.on("mousedown", (e) => {
            this._mouseState.leftDown = true;
            this._mouseState._leftPressedThisFrame = true;
        });
        app.renderer.plugins.interaction.on("mouseup", (e) => {
            this._mouseState.leftDown = false;
            this._mouseState._leftReleasedThisFrame = true;
        });
        app.renderer.plugins.interaction.on("rightdown", (e) => {
            this._mouseState.rightDown = true;
            this._mouseState._rightPressedThisFrame = true;
        });
        app.renderer.plugins.interaction.on("rightup", (e) => {
            this._mouseState.rightDown = false;
            this._mouseState._rightReleasedThisFrame = true;
        });

        // Touch
        app.renderer.plugins.interaction.on("touchstart", (e) => {
            // Check if two fingers
            if (e.data.originalEvent.touches.length > 1) {
                // Stop normal touching
                this._touchState.touching = false;
                this._touchState._finishedTouchingThisFrame = true;

                // Multitouch
                this._touchState.multiTouching = true;
                this._touchState.multiTouches = [...e.data.originalEvent.touches].map(touch => {
                    return Input.ScreenToWorldCoordinates(new Vector(touch.pageX, touch.pageY))
                })
            }
            else {
                this._touchState.touching = true;
                this._touchState._startedTouchingThisFrame = true;
                this._touchState.touchStartPoint = Input.ScreenToWorldCoordinates(new Vector(e.data.global));

                this._touchState.multiTouching = false
            }
            this._touchState.touchPosition = Input.ScreenToWorldCoordinates(new Vector(e.data.global));
        });

        app.renderer.plugins.interaction.on("touchmove", e => {
            if (e.data.originalEvent.touches.length > 1) {
                this._touchState.multiTouches = [...e.data.originalEvent.touches].map(touch => {
                    return Input.ScreenToWorldCoordinates(new Vector(touch.pageX, touch.pageY))
                })
            }
            else {
                this._touchState.touchPosition = Input.ScreenToWorldCoordinates(new Vector(e.data.global));
                this._touchState._touchMovedThisFrame = true;
            }
        });

        app.renderer.plugins.interaction.on("touchend", e => {
            if (e.data.originalEvent.touches.length === 1) {
                this._touchState.multiTouching = false;
            }
            else if (e.data.originalEvent.touches.length === 0) {
                this._touchState.touching = false;
                this._touchState._finishedTouchingThisFrame = true;
                this._touchState.touchPosition = Input.ScreenToWorldCoordinates(new Vector(e.data.global));
            }
            console.log("Ended touhces: ", e.data.originalEvent.touches.length)
        });
    }

    run() {
        // The params that start with _ are changing mid-frames, and the state has to be the same throughout the frame.
        this._mouseState.leftPressedThisFrame = this._mouseState._leftPressedThisFrame;
        this._mouseState.leftReleasedThisFrame = this._mouseState._leftReleasedThisFrame;
        this._mouseState.rightPressedThisFrame = this._mouseState._rightPressedThisFrame;
        this._mouseState.rightReleasedThisFrame = this._mouseState._rightReleasedThisFrame;
        this._touchState.startedDraggingThisFrame = this._touchState._startedDraggingThisFrame;
        this._touchState.finishedDraggingThisFrame = this._touchState._finishedDraggingThisFrame;
        this._touchState.startedTouchingThisFrame = this._touchState._startedTouchingThisFrame;
        this._touchState.finishedTouchingThisFrame = this._touchState._finishedTouchingThisFrame;
        this._touchState.touchMovedThisFrame = this._touchState._touchMovedThisFrame;

        this._mouseState._leftPressedThisFrame = false;
        this._mouseState._leftReleasedThisFrame = false;
        this._mouseState._rightPressedThisFrame = false;
        this._mouseState._rightReleasedThisFrame = false;
        this._touchState._startedDraggingThisFrame = false;
        this._touchState._finishedDraggingThisFrame = false;
        this._touchState._startedTouchingThisFrame = false;
        this._touchState._finishedTouchingThisFrame = false;
        this._touchState._touchMovedThisFrame = false;

        this._handleDrag();
    }

    _handleDrag() {
        if (this.leftMousePressedThisFrame) {
            this._mouseState.dragStartPoint = this.mouse;
            this._mouseState.dragging = true;
            this._mouseState._startedDraggingThisFrame = true;
        }

        if (this.leftMouseReleasedThisFrame) {
            this._mouseState.dragging = false;
            this._mouseState._finishedDraggingThisFrame = true;
        } else {
            this._mouseState._finishedDraggingThisFrame = false;
        }
    }

    isKeyPressed(key) {
        return this._pressedKeys.has(key);
    }

    _keyDown(key) {
        this._pressedKeys.add(key.key);
    }

    _keyUp(key) {
        this._pressedKeys.delete(key.key);
    }
};
