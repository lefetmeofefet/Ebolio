import System from "../../../../shared/systems/system.mjs";

export default new class Effects extends System {
    initialize() {
        // let lighting = new PIXI.display.Layer();
        // lighting.on('display', function (element) {
        //     element.blendMode = PIXI.BLEND_MODES.ADD
        // });
        // lighting.filters = [new PIXI.filters.VoidFilter()];
        // lighting.filters[0].blendMode = PIXI.BLEND_MODES.MULTIPLY;
        //
        // lighting.filterArea = app.screen;
        // lighting.filterArea = new PIXI.Rectangle(100, 100, 600, 400); //<-- try uncomment it
        //
        // app.stage.addChild(lighting);
        //
        // let ambient = new PIXI.Graphics();
        // ambient.beginFill(0x808080, 1.0);
        // ambient.drawRect(0, 0, app.renderer.width, app.renderer.height);
        // ambient.endFill();
        // lighting.addChild(ambient); //<-- try comment it


        // let filter = new PIXI.filters.BlurFilter();
        // filter.blur = 1;
        // app.stage.filters = [filter];


        var shaderFrag = `
        precision mediump float;
          
        uniform vec2 mouse;
        uniform vec2 resolution;
        uniform float time;
        vec4 color;
        
       
        
        void main() {
          //pixel coords are inverted in framebuffer
          vec2 pixelPos = vec2(gl_FragCoord.x, resolution.y - gl_FragCoord.y);
          if (length(mouse - pixelPos) < 125.0) {
              gl_FragColor = color;
          } else {
              gl_FragColor = vec4( sin(time), mouse.x/resolution.x, mouse.y/resolution.y, 0.2) * 0.2;
          }
        }
        `;


        // this.filter = new PIXI.Filter(null, shaderFrag);
        // app.stage.filters = [new PIXI.Filter(null, shaderFrag)];
    }

    initialize() {
    }

    run() {
        // let v2 = this.filter.uniforms.mouse;
        // let global = app.renderer.plugins.interaction.mouse.global;
        // v2[0] = global.x;
        // v2[1] = global.y;
        // this.filter.uniforms.mouse = v2;
        //
        // v2 = this.filter.uniforms.resolution;
        // v2[0] = app.renderer.width;
        // v2[1] = app.renderer.height;
        // this.filter.uniforms.resolution = v2;
    }
}
