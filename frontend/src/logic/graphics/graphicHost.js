import Host from "../../../../shared/entities/host.mjs";
import Config from "../../../../shared/configuration.mjs";
import app from "../game.js";
import {ClientGameState} from "../game.js"


export default class GraphicHost extends Host {
    get HOST_VISIBILITY_SCALE_LIMIT() {
        return 0.14;
    }

    get TEXT_RESIZE_SCALE_LIMIT() {
        return 0.7;
    }

    static Initialize() {
        const createHostTexture = function () {
            // return PIXI.loader.resources.hostSprite.texture;
            const graphics = new PIXI.Graphics();

            const layers = 5;
            // Outer circle
            graphics.lineStyle(3, 0xffffff, 1);
            // graphics.beginFill(0xffffff, 0.07);
            graphics.beginFill(0x000000, 1);
            graphics.drawCircle(Host.RADIUS, Host.RADIUS, Host.RADIUS - layers - 1);
            graphics.endFill();

            // outerouter circle
            for (let i = 0; i < layers; i++) {
                graphics.lineStyle(1, 0xffffff, i / layers);
                graphics.drawCircle(Host.RADIUS, Host.RADIUS, Host.RADIUS - i);
                graphics.endFill();
            }

            graphics.lineStyle(1, 0xffffff, 0.4);
            graphics.drawCircle(Host.RADIUS, Host.RADIUS, Host.RADIUS - layers - 3);
            graphics.endFill();

            const texture = new PIXI.RenderTexture(app.renderer, Host.RADIUS * 2, Host.RADIUS * 2);
            texture.render(graphics);
            return texture;
        };

        const createOutlineTexture = function () {
            const graphics = new PIXI.Graphics();

            graphics.lineStyle(4, 0x00dd00, 1);
            graphics.drawCircle(Host.RADIUS, Host.RADIUS, Host.RADIUS - 6);
            graphics.endFill();

            const texture = new PIXI.RenderTexture(app.renderer, Host.RADIUS * 2, Host.RADIUS * 2);
            texture.render(graphics);
            return texture;
        };

        const createSelectedTexture = function () {
            const graphics = new PIXI.Graphics();

            graphics.lineStyle(4, 0x00dd00, 1);
            graphics.beginFill(0x00dd00, 0.2);
            graphics.drawCircle(Host.RADIUS, Host.RADIUS, Host.RADIUS - 6);
            graphics.endFill();

            const texture = new PIXI.RenderTexture(app.renderer, Host.RADIUS * 2, Host.RADIUS * 2);
            texture.render(graphics);
            return texture;
        };

        // GraphicHost.HostsGraphicsParticleContainer = new PIXI.ParticleContainer(100000);
        GraphicHost.HostsGraphicsParticleContainer = new PIXI.DisplayObjectContainer();
        app.canvas.addChild(GraphicHost.HostsGraphicsParticleContainer);

        GraphicHost.HostsTextContainer = new PIXI.DisplayObjectContainer();
        app.canvas.addChild(GraphicHost.HostsTextContainer);

        GraphicHost.HostTexture = createHostTexture();
        GraphicHost.OutlineTexture = createOutlineTexture();
        GraphicHost.SelectedTexture = createSelectedTexture();

        GraphicHost.Initialized = true;
    }

    get highlighted() {
        return this._highlighted;
    }

    set highlighted(isHighlighted) {
        if (this._highlighted !== isHighlighted) {
            this._highlighted = isHighlighted;
            this._shouldCalculateTint = true;
        }
        else {
            this._highlighted = isHighlighted;
        }
    }

    get selected() {
        return this._selected;
    }

    set selected(isSelected) {
        if (this._selected !== isSelected) {
            this._selected = isSelected;
            this._shouldCalculateDisplayAccordingToScale = true;
            this._shouldCalculateTint = true;
        }
        else {
            this._selected = isSelected;
        }
    }

    setPlayerId(id) {
        super.setPlayerId(id);
        this._shouldCalculateTint = true;
    }

    constructor(state, params) {
        if (!GraphicHost.Initialized) {
            GraphicHost.Initialize();
        }

        super(state, params);

        this.mainSprite = new PIXI.Sprite(GraphicHost.HostTexture);
        this.mainSprite.width = Host.RADIUS * 2;
        this.mainSprite.height = Host.RADIUS * 2;

        this.virusesText = new PIXI.BitmapText("", {
            font: {
                name: "Roboto",
                size: 16,
                // tint: 0xffffff
            }
        });

        this.virusesText.anchor.x = 0.5;
        this.virusesText.anchor.y = 0.5;
        this.virusesText.x = this.mainSprite.width / 2;
        this.virusesText.y = this.mainSprite.height / 2;
        GraphicHost.HostsTextContainer.addChild(this.virusesText);

        this.updateGraphicsPosition();

        this.mainSprite.anchor.x = 0.5;
        this.mainSprite.anchor.y = 0.5;

        GraphicHost.HostsGraphicsParticleContainer.addChild(this.mainSprite);

        this.calculateDisplayAccordingToScale(app.scale);

        // EntityHider will make everything on the screen visible
        this.hide();
    }

    updateGraphicsPosition() {
        this.mainSprite.position.x = this.position.x;
        this.mainSprite.position.y = this.position.y;
        this.virusesText.position.x = this.position.x;
        this.virusesText.position.y = this.position.y;
    }

    update(state) {
        super.update(state);

        // Update num of viruses text
        let virusesText = Math.floor(this.viruses).toString();
        if (this.virusesText.text !== virusesText) {
            this._setVirusesText(virusesText);
        }

        if (this.scaleChanged && this.visible) {
            this.scaleChanged = false;
            this._shouldCalculateDisplayAccordingToScale = true;
        }

        if (this._shouldCalculateDisplayAccordingToScale) {
            this._shouldCalculateDisplayAccordingToScale = false;
            this.calculateDisplayAccordingToScale(app.scale);
        }
        if (this._shouldCalculateTint) {
            this._shouldCalculateTint = false;
            this.calculateTint();
        }
    }

    _setVirusesText(text) {
        this.virusesText.text = text;
    }

    calculateTint() {
        if (this.mainSprite) {
            let tint = ClientGameState.players[this.playerId].color;
            if (this._selected) {
                tint = 0x00ff00;
            }
            else if (this._highlighted) {
                tint = 0x00cc00;
            }
            this.mainSprite.tint = tint;
        }
    }

    calculateDisplayAccordingToScale(scale) {
        // If scaling far far
        if (scale < this.HOST_VISIBILITY_SCALE_LIMIT) {
            this.virusesText.tint = this._selected ? Config.SELECTION_COLOR : this.mainSprite.tint;
            this.virusesText.scale.set(1 / scale, 1 / scale);
            this.mainSprite.visible = false;
        }

        // If getting very close
        else if (scale > this.TEXT_RESIZE_SCALE_LIMIT) {
            this.virusesText.scale.set(1 / this.TEXT_RESIZE_SCALE_LIMIT, 1 / this.TEXT_RESIZE_SCALE_LIMIT);
            if (this.visible) {
                this.mainSprite.visible = true;
            }
        }

        // If in normal range
        else {
            this.virusesText.scale.set(1 / scale, 1 / scale);
            this.virusesText.tint = 0xffffff;
            if (this.visible) {
                this.mainSprite.visible = true;
            }
        }
    }

    hide() {
        this.mainSprite.visible = false;
        this.virusesText.visible = false;
        this.visible = false;
        for (let arm of this.arms) {
            arm.hide();
        }
    }

    show() {
        this._shouldCalculateDisplayAccordingToScale = true;
        this.virusesText.visible = true;
        this.visible = true;
        for (let arm of this.arms) {
            arm.show();
        }
    }

    destroy() {
        super.destroy();
        this.mainSprite.destroy();
        this.virusesText.destroy();
    }
}
