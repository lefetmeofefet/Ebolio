import GameState from "../../../shared/entities/gameState.mjs";
import NetworkEvents from "../../../shared/systems/networkEvents.mjs";
import Config from "../../../shared/configuration.mjs";
import PixiGame from "./pixiGame.js";
import GraphicHost from "./graphics/graphicHost.js";
import Networking from "./networking.js";
import Input from "./input.js";
import NavigationDesktop from "./systems/navigation/navigationDesktop.js";
import NavigationMobile from "./systems/navigation/navigationMobile.js";
import Interactivity from "./systems/interactivity.js";
import StaticInterface from "./systems/staticInterface.js";
import EntityHider from "./systems/entityHider.js";
import SyncSystem from "./systems/syncSystem.js";
import Effects from "./graphics/effects.js";

Config.IS_CLIENT = true;

let app = new PixiGame(0x101010);
window.app = app;
export default app;

let fpsMeter = null;

let ClientGameState = new class ClientGameState extends GameState {
    constructor(onNetworkEvent) {
        super(onNetworkEvent);
        app.addScaleListener((scale) => {
            for (let host of this.hosts.values()) {
                host.scaleChanged = true;
            }
        })
    }

    addHost(params) {
        super.addHost(params, GraphicHost);
    }

    sendNetworkEvent(event, value) {
        Networking.sendMessage({
            event: event,
            value: value
        });
    }
}(Networking.subscribe);

export {ClientGameState};

PIXI.loader
    .add("virusSpritesheet", "res/Virus2.json")
    // .add("hostSprite", "art/shlomo.jpg")
    .add("hostSprite", "res/Host.png")
    .add("Roboto", "res/roboto-bitmap.fnt")
    // .add("bmroboto", "res/font2/bmroboto.fnt")
    // .add("Roboto", "res/font/wat.fnt")
    .on("progress", loadProgressHandler)
    .load(setup);

function loadProgressHandler() {
    console.log("loading", arguments);
}

function setup() {
    fpsMeter = new FPSMeter({
        bottom: "5px",
        top: "auto"
    });
    if (!DevToolsOpen) {
        fpsMeter.hide();
    }

    let systems = [
        Input,
        NetworkEvents,
        window.IsMobile ? NavigationMobile : NavigationDesktop,
        Interactivity,
        StaticInterface,
        // VirusRepulsion,
        Effects,
        EntityHider,
        SyncSystem
    ];

    for (let system of systems) {
        system.initialize(ClientGameState);
    }

    app.startGame(function (delta) {
        if (!fpsMeter.isPaused) {
            fpsMeter.tickStart();
        }

        for (let system of systems) {
            system.run(ClientGameState);
        }
        ClientGameState.update();

        if (!fpsMeter.isPaused) {
            fpsMeter.tick();
        }
    });
}

/**
 * Enables the FPS counter, used for debugging
 */
function fps() {
    fpsMeter.show();
}
