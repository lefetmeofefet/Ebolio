import NetworkEvents from "../../../shared/systems/networkEvents.mjs";

export default new class Networking {
    constructor() {
        this.socket = io();
        this.socket.on("just_msg_mmmkay?", (message) => {
            console.log("Got message! ", JSON.parse(message));
            this.onMessage(JSON.parse(message));
        });
    }

    onMessage = message => {
        NetworkEvents.addEvent(message);
    };

    sendMessage(message) {
        this.socket.emit("just_msg_mmmkay?", JSON.stringify(message));
    }
};