import Config from "../../../shared/configuration.mjs";


export default class PixiGame {
    get renderer() {
        return this.app.renderer;
    }

    get stage() {
        return this.app.stage;
    }

    get pivot() {
        return this.gameContainer.pivot;
    }

    get scale() {
        return this.gameContainer.scale.x;
    }

    get canvas() {
        return this.gameContainer;
    }

    get zoom() {
        return this._zoom;
    }

    constructor(backgroundColor) {
        this.app = new PIXI.Application({
            antialias: true,
            transparent: false,
            width: window.innerWidth,
            height: window.innerHeight,
            resolution: 1
            // forceCanvas: true
        });
        window.onload = () => {
            document.body.appendChild(this.app.view);
        };

        this.app.renderer.view.style.position = "absolute";
        this.app.renderer.view.style.display = "block";
        this.app.renderer.autoResize = true;

        this._zoom = 1;
        this._scaleListeners = [];

        this._onResize();

        window.addEventListener('resize', () => this._onResize());
        if (backgroundColor) {
            this.app.renderer.backgroundColor = backgroundColor;
        }

        this.gameContainer = new PIXI.Container();

        // TODO: Decide, layers or naht
        // this.app.stage = new PIXI.Stage();
        this.app.stage.addChild(this.gameContainer);
        // this.app.stage.group.enableSort = true;
        this.app.stage.interactiveChildren = false;
        // this.initializeGameContentGroups();
    }

    initializeGameContentGroups() {
        // TODO: Decide, layers or naht
        this.interfaceGroup = new PIXI.display.Group(2, false);
        this.app.stage.addChild(new PIXI.display.Layer(this.interfaceGroup));

        this.backgroundGroup = new PIXI.display.Group(-1, false);
        this.app.stage.addChild(new PIXI.display.Layer(this.backgroundGroup));
    }

    addScaleListener(callback) {
        this._scaleListeners.push(callback);
    }

    setZoom(zoom) {
        this._zoom = zoom;
        this._onResize();
    }

    setScale(x, y) {
        this.gameContainer.scale.set(x, y);
        for (let listener of this._scaleListeners) {
            listener(x, y);
        }
    }

    getView() {
        return {
            x: this.gameContainer.pivot.x,
            y: this.gameContainer.pivot.y,
            width: this.app.renderer.screen.width / this.scale,
            height: this.app.renderer.screen.height / this.scale,
        }
    }

    setPivot(x, y) {
        this.gameContainer.pivot.set(x, y)
    }

    _onResize() {
        this.app.renderer.resize(window.innerWidth / this._zoom, window.innerHeight / this._zoom);
        this.app.view.style.width = window.innerWidth + "px";
        this.app.view.style.height = window.innerHeight + "px";
    }

    startGame(updateCallback) {
        this.app.ticker.add(function (delta) {
            updateCallback(delta);
        });
    }
}