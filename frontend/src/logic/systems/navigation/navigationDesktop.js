import Config from "../../../../../shared/configuration.mjs";
import Vector from "../../../../../shared/physics/vector.mjs"
import Input from "../../input.js";
import app from "../../game.js"
import System from "../../../../../shared/systems/system.mjs";
import NavigationBase from "./navigationBase.js";


export default new class NavigationDesktop extends NavigationBase {
    get KEYS_SCROLL_SPEED() {
        return 8 / this.scale;
    }

    get MOUSE_SCALE_SPEED_MULTIPLIER() {
        return 0.0018;
    }

    get KEYS_SCALE_SPEED() {
        return 0.05
    }

    _limitScale() {
        if (this._scaleDestination > Config.SCALE.MAX) {
            this._scaleDestination = Config.SCALE.MAX;
        } else if (this._scaleDestination < Config.SCALE.MIN) {
            this._scaleDestination = Config.SCALE.MIN;
        }
    }

    initialize(state) {
        super.initialize(state);
        this._scaleDestination = 1;
        this._scaleSpeed = 0;
        window.addEventListener('wheel', e => {
            this._zoomPoint = Input.mouse;
            if (e.deltaY < 0) {
                this.scaleIn(e.deltaY * this.MOUSE_SCALE_SPEED_MULTIPLIER);
            } else {
                this.scaleOut(e.deltaY * this.MOUSE_SCALE_SPEED_MULTIPLIER);
            }
        });
    }

    scaleOut(amount) {
        this._scaleDestination /= 1 + Math.abs(amount);
    }

    scaleIn(amount) {
        this._scaleDestination *= 1 + Math.abs(amount);
    }

    run(state) {
        this._limitScale();
        this._scaleSpeed += (this._scaleDestination / this.scale - 1) * 0.7;
        this._scaleSpeed *= 0.32;
        if (Math.abs(this._scaleSpeed) > 0.000001) {
            this._scale(this._scaleSpeed);
        }

        if (Input.isKeyPressed("w")) {
            this.center.set(this.center.x, this.center.y - this.KEYS_SCROLL_SPEED);
            this.shouldUpdateCenter = true;
        }
        if (Input.isKeyPressed("a")) {
            this.center.set(this.center.x - this.KEYS_SCROLL_SPEED, this.center.y);
            this.shouldUpdateCenter = true;
        }
        if (Input.isKeyPressed("s")) {
            this.center.set(this.center.x, this.center.y + this.KEYS_SCROLL_SPEED);
            this.shouldUpdateCenter = true;
        }
        if (Input.isKeyPressed("d")) {
            this.center.set(this.center.x + this.KEYS_SCROLL_SPEED, this.center.y);
            this.shouldUpdateCenter = true;
        }


        if (Input.dragging && Input.isKeyPressed("Control")) {
            let dragVector = Input.dragStartPoint.minus(Input.mouse);
            this.center.set(this.center.plus(dragVector));
            this.shouldUpdateCenter = true;
        }

        if (Input.isKeyPressed("ArrowDown")) {
            this.scaleOut(this.KEYS_SCALE_SPEED);
            this._zoomPoint = this.center;
        }
        if (Input.isKeyPressed("ArrowUp")) {
            this.scaleIn(this.KEYS_SCALE_SPEED);
            this._zoomPoint = this.center;
        }

        super.run(state);
    }

    _scale(scaleAmount) {
        let pointToFixBefore = this._zoomPoint;

        // Zoom to center
        let oldScale = this.scale;
        this.scale += scaleAmount * this.scale;

        app.setScale(this.scale, this.scale);

        this._calculatePivotByCenter();

        if (scaleAmount > 0) {
            // Mouse has to stay in the same XY coordinates as before, so we move center
            let pointToFixAfter = new Vector(
                ((pointToFixBefore.x - app.pivot.x) * oldScale) / this.scale + this.pivot.x,
                ((pointToFixBefore.y - app.pivot.y) * oldScale) / this.scale + this.pivot.y,
            );

            this.center.set(
                this.center.x + (pointToFixBefore.x - pointToFixAfter.x),
                this.center.y + (pointToFixBefore.y - pointToFixAfter.y)
            );

            this._calculatePivotByCenter();
        }

        this._updateAppPivot();
    }
};
