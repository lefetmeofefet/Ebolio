import Config from "../../../../../shared/configuration.mjs";
import Vector from "../../../../../shared/physics/vector.mjs"
import app from "../../game.js"
import System from "../../../../../shared/systems/system.mjs";


export default class NavigationBase extends System {
    initialize(state) {
        this.center = new Vector(app.renderer.screen.width / 2, app.renderer.screen.height / 2);
        this.pivot = new Vector(0, 0);
        this.scale = 1;
        this.shouldUpdateCenter = false;
        this.shouldUpdateScale = false;

        state.Navigation_center = this.center
    }

    run(state) {
        if (state.Navigation_recenter) {
            this.center.set(state.Navigation_recenter.x, state.Navigation_recenter.y);
            state.Navigation_recenter = null;
            this.shouldUpdateCenter = true;
        }

        if (this.shouldUpdateCenter) {
            // console.log("Shoud update center", state.recenter)
            this._calculatePivotByCenter();
            this._updateAppPivot();
            state.Navigation_center = this.center;
            this.shouldUpdateCenter = false;
        }
        if (this.shouldUpdateScale) {
            this._updateScale();
            this.shouldUpdateScale = false;
        }
    }

    _calculatePivotByCenter() {
        this.pivot.set(
            this.center.x - (app.renderer.screen.width / (2 * this.scale)),
            this.center.y - (app.renderer.screen.height / (2 * this.scale))
        );
    }

    _updateAppPivot() {
        app.setPivot(this.pivot.x, this.pivot.y);
    }

    _updateScale() {
        app.setScale(this.scale, this.scale);
    }
}
