import Config from "../../../../../shared/configuration.mjs";
import Vector from "../../../../../shared/physics/vector.mjs"
import Input from "../../input.js";
import app from "../../game.js"
import NavigationBase from "./navigationBase.js";


export default new class NavigationMobile extends NavigationBase {
    run(state) {
        this._handleSingleFingerDragging();
        this._handleMultiFingerNavigation();

        super.run(state);
    }

    _handleSingleFingerDragging() {
        if (Input.touching && Input.touchMovedThisFrame) {
            let dragVector = Input.touchStartPoint.minus(Input.touchPosition);
            this.center.set(this.center.plus(dragVector));
            this.shouldUpdateCenter = true;
        }
    }

    _handleMultiFingerNavigation() {
        // Two finger zooming
        if (Input.multiTouching) {
            if (this._firstMultiTouches) {
                let newMultiTouch = Input.multiTouches;
                let distanceStart = this._firstMultiTouches[0].distance(this._firstMultiTouches[1]);
                let distanceNow = newMultiTouch[0].distance(newMultiTouch[1]);
                let middlePointStart = Vector.Average([this._firstMultiTouches[0], this._firstMultiTouches[1]]);
                let middlePointNow = Vector.Average([newMultiTouch[0], newMultiTouch[1]]);

                // Zoom
                this.scale *= distanceNow / distanceStart;
                this.shouldUpdateScale = true;

                // Move center
                let dragVector = middlePointStart.minus(middlePointNow);
                this.center.set(this.center.plus(dragVector));
                this.shouldUpdateCenter = true;
            }
            else {
                this._firstMultiTouches = Input.multiTouches
            }
        }
        else {
            this._firstMultiTouches = null
        }
    }
};
