import Config from "../../../../shared/configuration.mjs";
import Navigation from "./navigation/navigationDesktop.js"
import app from "../game.js";
import System from "../../../../shared/systems/system.mjs";


export default new class StaticInterface extends System {
    initialize() {
        this.container = new PIXI.Container();

        this.initializeCoordinates();
        this.initializeScoreboard();

        app.stage.addChild(this.container);
    }

    initializeCoordinates() {
        this.coordinatesText = new PIXI.Text('');
        this.coordinatesText.style.fill = 0xbbbbbb;
        this.coordinatesText.style.fontSize = 20;
        this.coordinatesText.x = 10;
        this.coordinatesText.y = 10;
        this.container.addChild(this.coordinatesText);
    }

    initializeScoreboard() {
        this.scoreboard = new PIXI.Container();
        this.container.addChild(this.scoreboard);
    }

    run(state) {
        // Coordinates
        let text = Math.floor(state.Navigation_center.x) + ", " + Math.floor(state.Navigation_center.y);
        if (this.coordinatesText.text !== text) {
            this._setCoordinatesText(text);
        }

        // Scoreboard
        if (state.frame % 200 === 0) {
            this._updateScoreboard(state);
        }
    }

    _setCoordinatesText(text) {
        this.coordinatesText.text = text;
    }

    _updateScoreboard(state) {
        for (let player of Object.values(state.players)) {
            player.score = 0;
        }
        for (let host of state.hosts.values()) {
            state.players[host.playerId].score += host.viruses;
        }

        let sortedPlayers = Object.values(state.players).sort(player => -player.score);

        for (let child of this.scoreboard.children) {
            child.destroy();
        }
        this.scoreboard.removeChildren();
        let y = 10;
        for (let player of sortedPlayers) {
            if (player.id !== Config.NEUTRAL_PLAYER.ID){
                let score = new PIXI.Text(Math.floor(player.score).toString());
                score.style.fill = player.color;
                score.style.fontSize = 20;
                score.x = app.renderer.screen.width - score.width - 10;
                score.y = y;
                this.scoreboard.addChild(score);
                y += 30
            }
        }
    }
};
