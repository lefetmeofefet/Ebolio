import Host from "../../../../shared/entities/host.mjs";
import app from "../game.js"
import System from "../../../../shared/systems/system.mjs";


export default new class EntityHider extends System {
    initialize() {
        this.lastVisibleHosts = [];
    }

    run(state) {
        if (state.frame % 10 !== 0) {
            return
        }
        let screenRect = app.getView();
        screenRect.x -= 200 / app.scale;
        screenRect.width += 400 / app.scale;
        screenRect.y -= 200 / app.scale;
        screenRect.height += 400 / app.scale;

        // For debugging off-screen rendering
        // screenRect.x += 200;
        // screenRect.width -= 400;
        // screenRect.y += 200;
        // screenRect.height -= 400;
        this.updateHiddenHosts(state, screenRect);
    }

    updateHiddenHosts(state, screenRect) {
        let hostsInScreen = state.hostsGrid.getEntitiesInRect(screenRect.x, screenRect.y,
            screenRect.x + screenRect.width, screenRect.y + screenRect.height);

        // The following logic: take all hosts in screen and make them visible, and then take all the visible hosts from
        // last frame, and if they're not currently visible make them invisible.
        let lastVisibleHosts = this.lastVisibleHosts;
        this.lastVisibleHosts = [];

        for (let host of hostsInScreen) {
            host.show();
            host.isInScreen = true;
            this.lastVisibleHosts.push(host);
        }
        for (let host of lastVisibleHosts) {
            if (!host.isInScreen) {
                host.hide();
            }
            host.isInScreen = false;
        }
        for (let host of this.lastVisibleHosts) {
            host.isInScreen = false;
        }
    }

    _isEntityInScreen(entity, radius, screenRect) {
        return (entity.position.x + radius >= screenRect.x && entity.position.y + radius >= screenRect.y &&
            entity.position.x - radius <= screenRect.x + screenRect.width && entity.position.y - radius <= screenRect.y + screenRect.height);
    }
};
