import Host from "../../../../shared/entities/host.mjs";
import Vector from "../../../../shared/physics/vector.mjs";
import Input from "../input.js";
import Config from "../../../../shared/configuration.mjs";
import app from "../game.js";
import System from "../../../../shared/systems/system.mjs";


export default new class Interactivity extends System {
    initialize() {
        this.initializeSelectionRectangle();
    }

    initializeSelectionRectangle() {
        this.selectionRectangle = new PIXI.Graphics();
        this.selectionRectangle.parentGroup = app.interfaceGroup;
        app.canvas.addChild(this.selectionRectangle);
    }

    run(state) {
        if (window.IsMobile) {
            this._handleMobileHostSelection(state);
        } else {
            this._handleHostSelection(state);
        }
    }

    _handleMobileHostSelection(state) {
        let pressed = Input.touching;
        let released = Input.finishedTouchingThisFrame;
        let selectedHosts = [];
        let touchedHostWasSelected = false;

        if (released) {
            let touchedHost;
            for (let host of state.hosts.values()) {
                if (this._isMouseInHost(Input.touchPosition, host)) {
                    touchedHost = host;
                    touchedHostWasSelected = touchedHost.selected;
                }

                if (host.selected) {
                    selectedHosts.push(host);
                }

                host.selected = false;
            }

            if (touchedHost) {
                // If touched my host
                if (touchedHost.playerId === state.playerId) {
                    // If touched friendly host for sending, not selecting
                    if (selectedHosts.length > 0 && !selectedHosts.includes(touchedHost)) {
                        for (let selectedHost of selectedHosts) {
                            this._sendArm(state, selectedHost, touchedHost);
                        }
                    }
                    // If pressed my host
                    else {
                        touchedHost.selected = !touchedHostWasSelected;
                        if (touchedHostWasSelected) {
                            this._retractArms(state, touchedHost);
                        }
                    }
                }
                // If touched enemy host
                else {
                    for (let selectedHost of selectedHosts) {
                        this._sendArm(state, selectedHost, touchedHost);
                    }
                }
            }
        }
    }

    _handleHostSelection(state) {
        this._handleKeyboardShortcuts(state);

        this.mouse = Input.mouse;
        let hostInMouse;
        let selectedHosts = [];
        let isMouseInSelected = false;

        // Dont interact if control pressed
        if (Input.isKeyPressed("Control")) {
            this.selectionRectangle.clear();
            for (let host of state.hosts.values()) {
                host.highlighted = false;
            }
            return
        }

        // Highlight and find pointed host / hosts
        for (let host of state.hosts.values()) {
            let mouseInHost = this._isMouseInHost(this.mouse, host);
            host.highlighted = mouseInHost;
            if (mouseInHost) {
                hostInMouse = host;
                if (host.selected) {
                    isMouseInSelected = true;
                }
            }

            if (host.selected) {
                selectedHosts.push(host);
            }
        }

        // On left click
        if (Input.leftMousePressedThisFrame) {
            if (Input.isKeyPressed("Shift")) {
                if (hostInMouse) {
                    if (hostInMouse.playerId === state.playerId) {
                        this.ignoreNextRelease = true;
                        hostInMouse.selected = !hostInMouse.selected;
                    }
                }
            } else {
                // If there are no selected hosts
                if (selectedHosts.length === 0) {
                    if (hostInMouse) {
                        this._selectHostInMouse(state, hostInMouse);
                    }
                }
                // If there are selectedHosts but no host in mouse
                else if (!hostInMouse) {
                    for (let selectedHost of selectedHosts) {
                        selectedHost.selected = false;
                    }
                }
                // If host in mouse is selected
                else if (hostInMouse.selected) {
                    for (let selectedHost of selectedHosts) {
                        if (selectedHost !== hostInMouse) {
                            selectedHost.selected = false;
                        }
                    }
                }
            }
        }

        this._handleDraggingSelection(state);

        // On release
        if (Input.leftMouseReleasedThisFrame && !Input.isKeyPressed("Control")) {
            if (this.ignoreNextRelease) {
                this.ignoreNextRelease = false;
            } else if (!isMouseInSelected && hostInMouse && !hostInMouse.selected) {
                for (let selectedHost of selectedHosts) {
                    this._sendArm(state, selectedHost, hostInMouse);
                    selectedHost.selected = Input.isKeyPressed("Shift");
                }
            }
        }

        if (Input.rightMouseReleasedThisFrame) {
            if (hostInMouse) {
                this._retractArms(state, hostInMouse);
            }
        }
    }

    _handleDraggingSelection(state) {
        // Find hosts in selection
        let hostsInSelectionRectangle = [];
        if (Input.dragging || Input.finishedDraggingThisFrame) {
            hostsInSelectionRectangle = this._getHostsInSelectionRectangle(state);
            for (let hostInSelectionRectangle of hostsInSelectionRectangle) {
                hostInSelectionRectangle.highlighted = true;
            }
        }

        // On finish dragging
        if (Input.finishedDraggingThisFrame) {
            for (let host of hostsInSelectionRectangle) {
                host.selected = true;
            }
        }

        // Selection rectangle
        if (Input.dragging) {
            this._drawSelectionRect(Input.dragStartPoint, Input.mouse);
        } else {
            this.selectionRectangle.clear();
        }
    }

    _handleKeyboardShortcuts(state) {
        // Recenter
        if (Input.isKeyPressed(".") || Input.isKeyPressed(" ")) {
            let maxViruses = 0;
            let selectedHost;
            for (let host of state.hosts.values()) {
                host.selected = false;
                if (host.playerId === state.playerId && host.viruses > maxViruses) {
                    maxViruses = host.viruses;
                    selectedHost = host;
                    state.Navigation_recenter = host.position;
                }
            }
            if (selectedHost) {
                selectedHost.selected = true;
                return;
            }
        } else if (Input.isKeyPressed("Escape")) {
            // Deselect all
            for (let host of state.hosts.values()) {
                host.selected = false;
            }
        }
    }

    _getHostsInSelectionRectangle(state) {
        let minX = Math.min(Input.dragStartPoint.x, this.mouse.x);
        let minY = Math.min(Input.dragStartPoint.y, this.mouse.y);
        let maxX = Math.max(Input.dragStartPoint.x, this.mouse.x);
        let maxY = Math.max(Input.dragStartPoint.y, this.mouse.y);
        if (minX === maxX && minY === maxY) {
            return [];
        }

        let hosts = [];
        for (let host of state.hosts.values()) {
            if (host.playerId === state.playerId) {
                // Check if host is inside
                if (host.position.x + Host.RADIUS >= minX && host.position.x - Host.RADIUS <= maxX &&
                    host.position.y + Host.RADIUS >= minY && host.position.y - Host.RADIUS <= maxY) {

                    // Check if the whole selection rectangle is inside host, then don't select
                    if (!(
                        minX >= host.position.x - Host.RADIUS && maxX < host.position.x + Host.RADIUS &&
                        minY >= host.position.y - Host.RADIUS && maxY < host.position.y + Host.RADIUS)) {

                        // Check if host is actually not in selection rectangle, because of circular shape
                        if ((host.position.x >= minX && host.position.x <= maxX) ||
                            (host.position.y >= minY && host.position.y <= maxY)) {
                            hosts.push(host);
                        }

                        // Check if host is close enough to one of the vertices
                        else if (Vector.Distance(host.position, {x: minX, y: minY}) <= Host.RADIUS ||
                            Vector.Distance(host.position, {x: maxX, y: minY}) <= Host.RADIUS ||
                            Vector.Distance(host.position, {x: minX, y: maxY}) <= Host.RADIUS ||
                            Vector.Distance(host.position, {x: maxX, y: maxY}) <= Host.RADIUS) {
                            hosts.push(host);
                        }
                    }
                }
            }
        }
        return hosts;
    }

    _drawSelectionRect(p1, p2) {
        this.selectionRectangle.clear();
        this.selectionRectangle.lineStyle(1 / app.scale, 0x00dd00, 1);
        this.selectionRectangle.beginFill(0x00dd00, 0.2);
        this.selectionRectangle.drawRect(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y);
        this.selectionRectangle.endFill();
    }

    _selectHostInMouse(state, hostInMouse) {
        hostInMouse.selected = hostInMouse.playerId === state.playerId;
    }

    _sendArm(state, sourceHost, destinationHost) {
        sourceHost.sendArm(state, destinationHost.id);

        state.sendNetworkEvent(Config.NETWORK_EVENTS.VIRUSES_SENT, {
            source: sourceHost.id,
            destinationHostId: destinationHost.id
        });
    }

    _retractArms(state, sourceHost) {
        sourceHost.retractArms();

        state.sendNetworkEvent(Config.NETWORK_EVENTS.VIRUSES_STOP, {
            source: sourceHost.id
        });
    }

    _isMouseInHost(mouse, host) {
        return host.isPointInside(mouse);
    }
};
