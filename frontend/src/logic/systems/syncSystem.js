import System from "../../../../shared/systems/system.mjs";

export default new class SyncSystem extends System{
    run(state) {
        state.initializeSyncData();
    }
};
