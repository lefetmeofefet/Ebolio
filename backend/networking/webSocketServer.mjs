import socketIo from 'socket.io';
import Config from '../../shared/configuration.mjs';
import NetworkEvents from "../../shared/systems/networkEvents.mjs";


export default new class webSocketServer{
    start(webServer) {
        this.io = socketIo.listen(webServer);

        this.io.sockets.on('connection', (socket) => {
            socket.removeAllListeners();
            this.onMessage({
                event: Config.NETWORK_EVENTS.PLAYER_CONNECTED,
                value: {
                    socket: socket,
                    player: {
                        id: socket.id
                    }
                }
            });

            socket.on('disconnect', () => {
                this.onMessage({
                    event: Config.NETWORK_EVENTS.PLAYER_DISCONNECTED,
                    value: {
                        socket: socket,
                        id: socket.id
                    }
                });
            });

            socket.on('just_msg_mmmkay?', (message) => {
                let parsedMessage = JSON.parse(message);
                this.broadcastToEveryoneExcept(parsedMessage, socket);
                this.onMessage(parsedMessage);
            });
        });

    }

    onMessage(message) {
        NetworkEvents.addEvent(message);
    };

    sendMessageToClient(message, clientSocket) {
        clientSocket.emit("just_msg_mmmkay?", JSON.stringify(message));
    }

    broadcast(message) {
        this.io.emit("just_msg_mmmkay?", JSON.stringify(message));
    }

    broadcastToEveryoneExcept(message, socket) {
        socket.broadcast.emit("just_msg_mmmkay?", JSON.stringify(message));
    }
};
