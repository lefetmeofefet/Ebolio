import express from 'express';

const app = express();

app.use(express.json());
app.use(express.static('.'));
app.use(express.static('frontend'));

const webServer = app.listen(process.env.PORT || 8081, function () {
    let {port, address} = webServer.address();
    if (address === "::") {
        address = "http://localhost"
    }
    console.log(`Ebolio is UP! ${address}:${port}`)
});
export default webServer
