import Config from "../../shared/configuration";
import Networking from "../networking/webSocketServer";
import System from "../../shared/systems/system";


export default new class SyncSystem extends System {
    run(state) {
        if (state.syncData.hostsToUpdate.size > 0) {
            this.sendHostsState(state);
        }

        if (state.syncData.playersToAdd.length > 0) {
            this.addPlayers(state);
        }

        if (state.syncData.playersToRemove.length > 0) {
            this.removePlayers(state);
        }

        state.initializeSyncData();
    }

    sendHostsState(state) {
        this.broadcast(Config.NETWORK_EVENTS.HOST_STATE, [...state.syncData.hostsToUpdate]);
    }

    addPlayers(state) {
        for (let socket of state.syncData.playersToAdd) {
            this.sendNetworkEventToClient(
                Config.NETWORK_EVENTS.INIT,
                {
                    state: {
                        hosts: [...state.hosts.values()],
                        players: state.players
                    },
                    playerId: socket.id
                },
                socket
            );
            this.broadcastToEveryoneExcept(
                Config.NETWORK_EVENTS.PLAYER_CONNECTED,
                {
                    player: state.players[socket.id]
                },
                socket
            );
        }
    }

    removePlayers(state) {
        for (let socket of state.syncData.playersToRemove) {
            this.broadcast(
                Config.NETWORK_EVENTS.PLAYER_DISCONNECTED,
                {
                    id: socket.id
                },
                socket
            );
        }
    }

    sendNetworkEventToClient(event, value, clientSocket) {
        Networking.sendMessageToClient({
            event: event,
            value: value
        }, clientSocket);
    }

    broadcastToEveryoneExcept(event, value, socket) {
        Networking.broadcastToEveryoneExcept({
            event: event,
            value: value
        }, socket)
    }

    broadcast(event, value) {
        Networking.broadcast({
            event: event,
            value: value
        })
    }
};
