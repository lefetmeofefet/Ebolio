import Config from "../../shared/configuration";
import Vector from "../../shared/physics/vector";
import Host from "../../shared/entities/host";
import System from "../../shared/systems/system";


export default new class WorldBuilder extends System {
    initialize(state) {
        this.buildWorld(state);
    }

    buildHierarchicalWorld(state) {

        let bodyLayer = [];
        let limbLayer = [];
        let tissueLayer = [];
        let cellLayer = [];

        // let rows = 5;
        // let initialRowSize = 3;
        // let finalLocations = [];
        // let lastRow = [];
        // for (let row = 0; row < rows; row += 1) {
        //     let thisRow = [];
        //     for (let col = 0; col < initialRowSize; col += 1) {
        //         thisRow.push(WorldBuilder.createVertex(row, col, lastRow, thisRow, row < rows / 2 + 1));
        //     }
        //     if (row < rows / 2) {
        //         initialRowSize += 1;
        //     }
        //     else {
        //         initialRowSize -= 1;
        //     }
        //
        //     for (let vertex of thisRow) {
        //         finalLocations.push(vertex);
        //     }
        //     lastRow = thisRow;
        // }

        let finalLocations = new Array(1000).fill(0).map(() => {
            return {
                fixed: false,
                connections: [],
                position: new Vector(Math.random() * 20 - 10, Math.random() * 20 - 10),
                velocity: new Vector(0, 0)
            }
        });

        let windowSize = 6;
        let count = 0;
        let windowSizeGoingUp = true;
        let maxWindowSize = 13;
        let minWindowSize = 1;

        for (let i=0; i<finalLocations.length - 1; i++) {
            for (let j = i + 1; j < Math.min(i + windowSize, finalLocations.length); j++) {
                finalLocations[i].connections.push(finalLocations[j]);
                finalLocations[j].connections.push(finalLocations[i]);

            }

            count += 1;
            if (count % 2 == 0) {
                if (windowSizeGoingUp) {
                    windowSize += 1;
                    if (windowSize == maxWindowSize) {
                        windowSizeGoingUp = false;
                        windowSize -= 1;
                    }
                }
                else {
                    windowSize -= 1;
                    if (windowSize == minWindowSize) {
                        windowSizeGoingUp = true;
                        windowSize += 1;
                    }
                }
            }
        }

        this.forceSimulation(finalLocations);

        for (let vertex of finalLocations) {
            state.addHost({
                position: vertex.position,
                maxViruses: 100
            });
        }
    }

    static createVertex(row, col, lastRow, thisRow, isFirstHalf) {
        let vertex = {
            fixed: false,
            connections: [],
            position: new Vector(row * 300, col * 300),
            velocity: new Vector(0, 0)
        };
        if (lastRow[col]) {
            vertex.connections.push(lastRow[col]);
            lastRow[col].connections.push(vertex);
        }
        if (isFirstHalf && lastRow[col - 1]) {
            vertex.connections.push(lastRow[col - 1]);
            lastRow[col - 1].connections.push(vertex);
        }
        if (!isFirstHalf && lastRow[col + 1]) {
            vertex.connections.push(lastRow[col + 1]);
            lastRow[col + 1].connections.push(vertex);
        }

        if (col > 0) {
            vertex.connections.push(thisRow[col - 1]);
            thisRow[col - 1].connections.push(vertex);
        }
        return vertex;
    }

    forceSimulation(vertices) {
        for (let i=0; i<1000; i++) {
            for (let vertex of vertices) {
                WorldBuilder.calculateVertexForce(vertex);
            }

            for (let vertex of vertices) {
                WorldBuilder.calculateVertexForce(vertex);
                WorldBuilder.applyVertexForce(vertex);
            }
        }
    }

    static calculateVertexForce(vertex) {
        for (let connectedVertex of vertex.connections) {
            let diffVector = connectedVertex.position.minus(vertex.position);
            let distance = diffVector.magnitude();
            let defaultDistance = 10000;
            let distanceOffset = distance - defaultDistance;

            diffVector.setMagnitude(distanceOffset * 0.01);
            vertex.velocity.add(diffVector)
        }
    }
    static applyVertexForce(vertex) {
        vertex.velocity = vertex.velocity.multiply(0.99);
        vertex.position.add(vertex.velocity);
        vertex.velocity.set(0, 0);
    }








    buildWorld(state) {
        for (let i = 0; i < 2000; i++) {
            let newPosition;
            while (true) {
                newPosition = this._generateRandomHostLocation();
                if (![...state.hosts.values()].some(host => Vector.Distance(host.position, newPosition) < Host.RADIUS * 2.2)) {
                    break;
                }
            }

            let maxViruses = 10 + (30000 * Math.random() * Math.random()) / (100 + Vector.Magnitude(newPosition));
            state.addHost({
                position: newPosition,
                maxViruses: maxViruses
            });
        }
    }

    _generateRandomHostLocation() {
        let rand = Math.random();
        let distance = rand * rand * Config.RESOLUTION.x * 40;
        let angle = Math.random() * Math.PI * 2;
        return {
            x: Math.cos(angle) * distance,
            y: Math.sin(angle) * distance
        }
    }
};
