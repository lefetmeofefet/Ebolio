import startGameloop from './gameloop.mjs';
import webStaticServer from "./networking/webStaticServer.mjs"
import webSocketServer from "./networking/webSocketServer";
import Config from "../shared/configuration.mjs";
Config.IS_CLIENT = false;

webSocketServer.start(webStaticServer);

startGameloop();

