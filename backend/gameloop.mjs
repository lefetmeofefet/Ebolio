import gameloop from 'node-gameloop';
import SyncSystem from './systems/syncClients.mjs';
import WorldBuilder from './systems/worldBuilder.mjs';
import GameState from '../shared/entities/gameState.mjs';
import NetworkEvents from '../shared/systems/networkEvents.mjs';

const FPS = 60;

export default function startGameloop() {
    let ServerGameState = new GameState();

    let systems = [
        WorldBuilder,
        NetworkEvents,
        SyncSystem
    ];

    for (let system of systems) {
        system.initialize(ServerGameState)
    }

    const id = gameloop.setGameLoop(function (delta) {
        for (let system of systems) {
            system.run(ServerGameState)
        }
        ServerGameState.update();
    }, 1000 / FPS);
}
