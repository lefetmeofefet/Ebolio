export default {
    IS_CLIENT: true,
    RESOLUTION: {
        x: 600,
        y: 600
    },
    NETWORK_EVENTS: {
        PLAYER_CONNECTED: "playerConnected",
        PLAYER_DISCONNECTED: "playerDisconnected",
        INIT: "init",
        HOST_STATE: "HostState",
        PLAYERS_AND_HOSTS_STATE: "PlayersAndHostState",
        VIRUSES_SENT: "VirusesSent",
        VIRUSES_STOP: "VirusesStop"
    },
    NEUTRAL_PLAYER: {
        ID: "1337NEUTRAL1337",
        COLOR: 0xb8b78d, // Georgian green
        GROWTH_MULTIPLIER: 0.007
    },
    PLAYER_GROWTH_MULTIPLIER: 0.01,
    SCALE: {
        MAX: 1,
        MIN: 0.03
    },
    SELECTION_COLOR: 0x00dd00
};
