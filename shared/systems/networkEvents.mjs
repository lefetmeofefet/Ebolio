import Config from "../configuration.mjs";
import System from "./system.mjs";


export default new class NetworkEvents extends System {
    constructor() {
        super();
        this._eventFunctionMapping = new Map();
        this._eventFunctionMapping.set(Config.NETWORK_EVENTS.INIT, this.handleInitGameEvent);
        this._eventFunctionMapping.set(Config.NETWORK_EVENTS.HOST_STATE, this.handleHostStateEvent);
        this._eventFunctionMapping.set(Config.NETWORK_EVENTS.PLAYERS_AND_HOSTS_STATE, this.handlePlayerAndHostsStateEvent);
        this._eventFunctionMapping.set(Config.NETWORK_EVENTS.VIRUSES_SENT, this.handleVirusesSentEvent);
        this._eventFunctionMapping.set(Config.NETWORK_EVENTS.VIRUSES_STOP, this.handleVirusesStopEvent);
        this._eventFunctionMapping.set(Config.NETWORK_EVENTS.PLAYER_CONNECTED, this.handlePlayerConnectedEvent);
        this._eventFunctionMapping.set(Config.NETWORK_EVENTS.PLAYER_DISCONNECTED, this.handlePlayerDisconnectedEvent);
        this._events = [];
    }

    addEvent(event) {
        this._events.push(event);
    }

    run(state) {
        let networkEvent = this._events.shift();
        while (networkEvent) {
            this._eventFunctionMapping.get(networkEvent.event)(state, networkEvent.value);
            networkEvent = this._events.shift();
        }
    }


    handleInitGameEvent(state, initGameEvent) {
        // Only happens for client
        let serverGameState = initGameEvent.state;

        state.players = serverGameState.players;

        if (initGameEvent.playerId) {
            state.playerId = initGameEvent.playerId;
            if (Config.IS_CLIENT) {
                state.Navigation_recenter = serverGameState.hosts.find(host => host.playerId === state.playerId).position;
            }
        }


        for (let hostId of state.hosts.keys()) {
            state.removeHost(hostId);
        }
        for (let host of serverGameState.hosts) {
            state.addHost(host);
        }
    }

    handleHostStateEvent(state, hostsStateEvent) {
        let nonexistentHosts = [];
        for (let newHost of hostsStateEvent) {
            let matchingHost = state.hosts.get(newHost.id);
            if (matchingHost) {
                // Unselect host if it's been captured
                if (Config.IS_CLIENT && newHost.playerId !== matchingHost.playerId) {
                    matchingHost.selected = false;
                }

                matchingHost.init(state, newHost)
            } else {
                nonexistentHosts.push(newHost)
            }
        }

        for (let host of nonexistentHosts) {
            state.addHost(host);
        }
    }

    handlePlayersStateEvent(state, playersStateEvent) {
        let nonexistentPlayers = [];
        for (let newPlayer of playersStateEvent) {
            let matchingPlayer = state.players.find(player => player.id === newPlayer.id);
            if (matchingPlayer) {
                matchingPlayer.color = newPlayer.color;
            } else {
                nonexistentPlayers.push(newPlayer)
            }
        }

        for (let player of nonexistentPlayers) {
            state.addPlayer(player);
        }
    }

    handlePlayerAndHostsStateEvent(state, playerAndHostsStateEvent) {
        this.handleHostStateEvent(state, playerAndHostsStateEvent.hosts);
        this.handlePlayersStateEvent(state, playerAndHostsStateEvent.players);
    }

    handleVirusesSentEvent(state, virusesSentEvent) {
        let hostThatSentViruses = state.hosts.get(virusesSentEvent.source);
        if (hostThatSentViruses) {
            hostThatSentViruses.sendArm(state, virusesSentEvent.destinationHostId);
        }
    }

    handleVirusesStopEvent(state, virusesStopEvent) {
        let hostThatStoppedViruses = state.hosts.get(virusesStopEvent.source);
        if (hostThatStoppedViruses) {
            hostThatStoppedViruses.retractArms();
        }
    }

    handlePlayerConnectedEvent(state, playerConnectedEvent) {
        let socket = playerConnectedEvent.socket;
        let player = playerConnectedEvent.player;
        state.addPlayer(player);

        for (let host of state.hosts.values()) {
            if (host.playerId === Config.NEUTRAL_PLAYER.ID) {
                host.setPlayerId(player.id);
                break;
            }
        }

        state.syncData.playersToAdd.push(socket);
    }

    handlePlayerDisconnectedEvent(state, playerDisconnectedEvent) {
        let socket = playerDisconnectedEvent.socket;
        let playerId = playerDisconnectedEvent.id;
        state.removePlayer(playerId);
        for (let host of state.hosts.values()) {
            if (host.playerId === playerId) {
                host.setNeutral();
            }
        }

        state.syncData.playersToRemove.push(socket);
    }
};
