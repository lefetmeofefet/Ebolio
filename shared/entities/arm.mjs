import Vector from "../physics/vector.mjs";
import Config from "../configuration.mjs";
import {guid} from "../utils.mjs";
import Host from "./host.mjs";


export default class Arm {
    static get SPEED() {
        return 3
    }

    static Initialize() {
        const createArmTexture = function () {
            // return PIXI.loader.resources.hostSprite.texture;
            const graphics = new PIXI.Graphics();
            const color =0x80a0ff;

            // graphics.lineStyle(1 / app.scale, 0x00dd00, 1);
            graphics.beginFill(color, 0.2);
            graphics.drawRect(0, 0, 3, 1);
            graphics.endFill();

            graphics.beginFill(color, 0.8);
            graphics.drawRect(0, 1, 3, 1);
            graphics.endFill();

            graphics.beginFill(color, 0.2);
            graphics.drawRect(0, 2, 3, 1);
            graphics.endFill();

            const texture = new PIXI.RenderTexture(app.renderer, 3, 3);
            texture.render(graphics);
            return texture;
        };

        const createArrowTexture = function () {
            const graphics = new PIXI.Graphics();
            const color =0x80a0ff;

            // graphics.lineStyle(1 / app.scale, 0x00dd00, 1);
            graphics.beginFill(color, 0.8);
            graphics.lineStart(0, 0);
            graphics.lineTo(1, 1);
            graphics.lineTo(2, 0);
            graphics.endFill();

            const texture = new PIXI.RenderTexture(app.renderer, 3, 3);
            texture.render(graphics);
            return texture;
        };

        // GraphicHost.HostsGraphicsParticleContainer = new PIXI.ParticleContainer(100000);
        Arm.ArmsGraphicsParticleContainer = new PIXI.DisplayObjectContainer();
        app.canvas.addChild(Arm.ArmsGraphicsParticleContainer);

        Arm.ArmTexture = createArmTexture();
        // Arm.ArrowTexture = createArrowTexture();
        Arm.Initialized = true;
    }

    constructor(state, {id, sourceHostId, destinationHostId, length = 0, reachedDestination = false}) {
        if (!Arm.Initialized && Config.IS_CLIENT) {
            Arm.Initialize();
        }

        if (id) {
            this.id = id;
        }
        else {
            this.id = guid();
        }

        this.sourceHostId = sourceHostId;
        this.destinationHostId = destinationHostId;

        // Calculate starting and ending points according to host radius
        let sourceHost = state.getHostById(this.sourceHostId);
        let destinationHost = state.getHostById(this.destinationHostId);
        let sourcePosition = sourceHost.position;
        let destinationPosition = destinationHost.position;
        let diffVector = Vector.Minus(destinationPosition, sourcePosition);
        sourcePosition = Vector.Plus(sourcePosition, diffVector.resized(Host.RADIUS - 3));
        destinationPosition = Vector.Minus(destinationPosition, diffVector.resized(Host.RADIUS - 3));

        this.sourcePosition = sourcePosition;
        this.destinationPosition = destinationPosition;
        this.length = length;
        this.reachedDestination = reachedDestination;

        if (Config.IS_CLIENT) {
            this._initializeGraphics()
        }
    }

    _initializeGraphics() {
        // Main sprite
        this.mainSprite = new PIXI.Sprite(Arm.ArmTexture);
        this.mainSprite.height = 7;
        // Width stretches
        this.mainSprite.width = 1;

        let diffVector = Vector.Minus(this.destinationPosition, this.sourcePosition);
        this.mainSprite.position = this.sourcePosition;
        this.mainSprite.rotation = Vector.Direction(diffVector);

        this.mainSprite.anchor.x = 0.0;
        this.mainSprite.anchor.y = 0.5;

        Arm.ArmsGraphicsParticleContainer.addChild(this.mainSprite);

        // Arrow
        // this.arrowSprite = new PIXI.Sprite(Arm.ArrowTexture);
        // this.arrowSprite.height = 20;
        // this.arrowSprite.width = 20;
        //
        // this.arrowSprite.rotation = Vector.Direction(diffVector);

        this.hide();
        this._updateGraphics();
    }

    update(state) {
        this.length += Arm.SPEED;
        if (this.length >= Vector.Distance(this.sourcePosition, this.destinationPosition)) {
            this.reachedDestination = true;
            state.syncData.hostsToUpdate.add(state.getHostById(this.destinationHostId));
        }

        if (Config.IS_CLIENT) {
            this._updateGraphics();
        }
    }

    _updateGraphics() {
        this.mainSprite.width = this.length;
        // let diffVector = Vector.Minus(this.destinationPosition, this.sourcePosition);
        // this.arrowSprite.position = Vector.Plus(this.sourcePosition, diffVector.resized(this.length));
    }

    hide() {
        this.mainSprite.visible = false;
    }

    show() {
        this.mainSprite.visible = true;
    }

    destroy() {
        if (Config.IS_CLIENT) {
            this.mainSprite.destroy();
        }
    }
}