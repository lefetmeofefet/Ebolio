import Host from "./host.mjs";
import SparseEntitiesGrid from"../physics/sparseEntitiesGrid.mjs";
import Config from"../configuration.mjs";


export default class GameState {
    constructor() {
        this.frame = 0;
        this.players = {};
        this.players[Config.NEUTRAL_PLAYER.ID] = {
            id: Config.NEUTRAL_PLAYER.ID,
            color: Config.NEUTRAL_PLAYER.COLOR,
            growthMultiplier: Config.NEUTRAL_PLAYER.GROWTH_MULTIPLIER
        };
        /**
         *
         * @type {Map<string, Host>}
         */
        this.hosts = new Map();
        this.hostsGrid = new SparseEntitiesGrid();

        // Free to use by any system
        this.syncData = null;
        this.initializeSyncData();
    }

    initializeSyncData() {
        this.syncData = {
            hostsToUpdate: new Set(),
            playersToAdd: [],
            playersToRemove: []
        };
    }

    addPlayer({id, color, growthMultiplier = Config.PLAYER_GROWTH_MULTIPLIER} = {}) {
        this.players[id] = {
            id: id,
            color: color || Math.floor(Math.random() * Math.pow(255, 3) - 1),
            growthMultiplier: growthMultiplier
        };
    }

    removePlayer(id) {
        delete this.players[id];
    }

    addHost(params, overrideHostClass) {
        let newHost;
        if (overrideHostClass) {
            newHost = new overrideHostClass(this, params);
        }
        else {
            newHost = new Host(this, params);
        }
        this.hosts.set(newHost.id, newHost);
        this.hostsGrid.addEntity(newHost, newHost.position.x, newHost.position.y);
    }

    removeHost(hostId) {
        let host = this.hosts.get(hostId);
        this.hostsGrid.removeEntity(host);
        this.hosts.delete(hostId);
        host.destroy();
    }

    getHostById(id) {
        return this.hosts.get(id);
    }

    update() {
        this.frame += 1;

        for (let host of this.hosts.values()) {
            host.update(this);
        }
    }
};
