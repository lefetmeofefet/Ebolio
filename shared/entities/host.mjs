import Vector from "../physics/vector.mjs";
import Config from "../configuration.mjs";
import {guid} from "../utils.mjs";
import Arm from "./arm.mjs";


export default class Host {
    static get RADIUS() {
        return 64;
    }

    static get SPAWNING_RATE() {
        return 0.3;
    }

    setPlayerId(id) {
        if (this.playerId !== id) {
            this.playerId = id;
            this.destroyArms();
            this.arms = [];
        }
    }

    destroyArms() {
        for (let arm of this.arms || []) {
            arm.destroy()
        }
        this.arms = [];
    }

    setNeutral() {
        this.setPlayerId(Config.NEUTRAL_PLAYER.ID);
    }

    constructor(state, params) {
        this.init(state, params)
    }

    init(state, {id, position, viruses = 0, playerId = Config.NEUTRAL_PLAYER.ID, maxViruses = 40, arms = []} = {}) {
        if (id) {
            this.id = id;
        } else {
            this.id = guid();
        }

        this.position = {};
        if (position) {
            this.position.x = position.x;
            this.position.y = position.y;
        } else {
            this.position.x = 50 + Math.random() * (Config.RESOLUTION.x - 100);
            this.position.y = 50 + Math.random() * (Config.RESOLUTION.y - 100);
        }

        this.maxViruses = maxViruses;
        this.setPlayerId(playerId);
        if (this.arms.length > 0) {
            this.destroyArms();
        }
        this.arms = this.createArms(state, arms);
        this.viruses = viruses;
    }

    createArms(state, arms) {
        return arms.map(armMetadata => new Arm(state, armMetadata));
    }

    update(state) {
        this.updateViruses(state);

        let destinationHostIds = [];
        for (let arm of this.arms) {
            if (!arm.reachedDestination) {
                arm.update(state)
            }
            else {
                destinationHostIds.push(arm.destinationHostId)
            }
        }

        // Check if sending anything
        if (destinationHostIds.length > 0) {
            let spawningRate = this.viruses >= Host.SPAWNING_RATE ? Host.SPAWNING_RATE : this.viruses;
            spawningRate /= destinationHostIds.length;
            for (let destinationHostId of destinationHostIds) {
                let destinationHost = state.getHostById(destinationHostId);
                this.viruses -= spawningRate;

                if (destinationHost.playerId === this.playerId) {
                    destinationHost.viruses += spawningRate;
                } else {
                    destinationHost.viruses -= spawningRate;
                    if (destinationHost.viruses <= 0) {
                        destinationHost.viruses = -destinationHost.viruses;
                        if (!Config.IS_CLIENT) {
                            destinationHost.setPlayerId(this.playerId);
                            state.syncData.hostsToUpdate.add(destinationHost);
                        }
                    }
                }
            }
        }
    }

    updateViruses(state) {
        if (this.viruses < this.maxViruses) {
            this.viruses += state.players[this.playerId].growthMultiplier;

            if (this.viruses > this.maxViruses) {
                this.viruses = this.maxViruses;
            }
        }
    }

    sendArm(state, destinationHostId) {
        if (!Config.IS_CLIENT) {
            this.arms.push(new Arm(state, {
                sourceHostId: this.id,
                destinationHostId: destinationHostId,
            }));

            state.syncData.hostsToUpdate.add(this)
        }
    }

    retractArms() {
        this.destroyArms();
    }


    isPointInside(point) {
        return point.distance(this.position) <= Host.RADIUS;
    }

    destroy() {
        this.destroyArms()
    }
};
