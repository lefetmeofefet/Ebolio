export default class SlowSparseEntitiesGrid {
    constructor(cellSize) {
        this.CELL_SIZE = cellSize || 130;
        // Map of stringified 2d coordinate -> Set of entities
        // NOTE: There could be same entities across different cells, because these entities are in both cells
        this.grid = new Map();
    }

    /**
     * Adds an entity into the sparse grid.
     * The grid will now have a reference to the entity from each cell that the entity intersects, based on (x, y) and radius.
     * The entity itself will have a parameter named populatedCells that will contain a list of all cells referencing it.
     * @param entity
     * @param x
     * @param y
     * @param radius
     */
    addEntity(entity, x, y, radius) {
        let xIndex = Math.floor(x / this.CELL_SIZE);
        let yIndex = Math.floor(y / this.CELL_SIZE);
        let cellStartX = xIndex * this.CELL_SIZE;
        let cellStartY = yIndex * this.CELL_SIZE;
        let cellEndX = (xIndex + 1) * this.CELL_SIZE;
        let cellEndY = (yIndex + 1) * this.CELL_SIZE;

        // Considering cell size isn't more than an entity's diameter, now we check each adjacent cell if entity is partially inside
        let containingCells = [{
            x: xIndex, y: yIndex
        }];
        let crossingRight = x + radius >= cellEndX;
        let crossingLeft = x - radius <= cellStartX;
        let crossingBottom = y + radius >= cellEndY;
        let crossingTop = y - radius <= cellStartY;

        if (crossingRight) {
            containingCells.push({x: (xIndex + 1), y: yIndex});
        }
        if (crossingLeft) {
            containingCells.push({x: (xIndex - 1), y: yIndex});
        }
        if (crossingBottom) {
            containingCells.push({x: xIndex, y: (yIndex + 1)});
        }
        if (crossingTop) {
            containingCells.push({x: xIndex, y: (yIndex - 1)});
        }

        // TODO: The current logic acts as if entity if a rectangle. Fix it, like in selection rectangle logic
        if (crossingRight && crossingBottom) {
            containingCells.push({x: (xIndex + 1), y: (yIndex + 1)});
        }
        if (crossingRight && crossingTop) {
            containingCells.push({x: (xIndex + 1), y: (yIndex - 1)});
        }
        if (crossingLeft && crossingBottom) {
            containingCells.push({x: (xIndex - 1), y: (yIndex + 1)});
        }
        if (crossingLeft && crossingTop) {
            containingCells.push({x: (xIndex - 1), y: (yIndex - 1)});
        }

        // Insert entity into all cells
        entity.populatedCells = [];
        for (let cell of containingCells) {
            let entitiesInCell = this.grid.get(cell.x + "," + cell.y);
            if (entitiesInCell) {
                entitiesInCell.add(entity);
            }
            else {
                let newCellContent = new Set();
                newCellContent.add(entity);
                this.grid.set(cell.x + "," + cell.y, newCellContent);
            }
            entity.populatedCells.push(cell.x + "," + cell.y);
        }
    }

    removeEntity(entity) {
        for (let cellString of entity.populatedCells) {
            let entitiesInCell = this.grid.get(cellString);
            if (entitiesInCell) {
                if (entitiesInCell.size === 1) {
                    this.grid.delete(cellString);
                }
                else {
                    entitiesInCell.delete(entity);
                }
            }
        }
        entity.populatedCells = null;
    }

    updateEntity(entity, x, y, radius) {
        this.removeEntity(entity);
        this.addEntity(entity, x, y, radius);
    }

    getEntitiesInCell(x, y) {
        let cellEntities = this.grid.get(Math.floor(x / this.CELL_SIZE) + "," + Math.floor(y / this.CELL_SIZE));
        return cellEntities || [];
    }

    getEntitiesInRect(startX, startY, endX, endY) {
        let entities = new Set();
        let startCellX = Math.floor(startX / this.CELL_SIZE);
        let startCellY = Math.floor(startY / this.CELL_SIZE);
        let endCellX = Math.floor(endX / this.CELL_SIZE);
        let endCellY = Math.floor(endY / this.CELL_SIZE);

        for (let x = startCellX; x <= endCellX; x++) {
            for (let y = startCellY; y <= endCellY; y++) {
                let cellEntities = this.grid.get(x + "," + y);
                if (cellEntities) {
                    for (let entity of cellEntities) {
                        entities.add(entity);
                    }
                }
            }
        }
        return entities;
    }
};
