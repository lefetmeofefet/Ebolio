Bugs:
- Disconnecting sometimes keeps the player. send keepalives sometimes.

Optimisations:
- Remove stupid layers, make menu and static displays html
- Remove all vector objects, leave only the static functions
- Index the space
- Let client know only what fog of war lets him know
- Consider moving to lower server FPS and time relative physics


Gameplay:
- Arrow to indicate continuous sending
- Allow selection and navigation of viruses
- Make viruses die by chance in the wild
- Add fog of war
- As you get closer to (0, 0) the hosts get more powerful and offensive. Give the game a goal, maybe consider alliances?
- Minimap
- Arc indicating how full a host is. full circle is fully full.
- Sending is always continuous, but allow percentage to not be sent always 

Social:
- Log in, player names
- history,
- chat, alliances


Controls:
- Selection groups
- Consider right click
- Ctrl drag navigation


Graphics:
- Make fullscreen
- Shaders
- Connecting lines between selection and target (for long distance shooting beyond screen)
- For presentation, record a background game with Ctrl+ 200% resolution, it looks amazing.

TESTS:
- Save crashes to mongo
- Crafted events (send from enemy hosts, initGameState to server)



ARCHITECTURE:
- Endless scalability: This could possibly be amazing.
We cannot expect O(n) loops of hosts / viruses to run in real-time.
    - Clientside logic must run on some portion of the entities which are close enough to the view
        - Clientside has to be able to query serverside about the state, when scrolling
    - Serverside logic must run on portion of the entities. this is a problem.
        -

No server instance has all the gamestate data, so it must be stored on some other server (either DB or RAM server)
    - Clientside actions have to be sent to a distribution server which will send the same message to other relevant clients
    - That distribution server will also send the message to the relevant game server

So far we have 3 server types:
    - Multiple game servers that handle part of the map
    - Single distribution server
    - Single data server

This is gonna be a major fustercluck.
